from __future__ import absolute_import
from __future__ import print_function
from pprint import pprint
from getpass import getpass
import argparse
import time
import json
import sys
import os
import re
SDK_PATH = os.path.join(os.path.dirname(__file__), "./splunk-sdk")
sys.path.insert(0, SDK_PATH)
# Import SDK
try:
    import splunklib.client as client
except:
    print("Unable to load Splunk SDK from %s" % SDK_PATH)
    sys.exit()


class CredentialManager:

    def __init__(self, hostname=None, port=8089, username=None, password=None, app=None):
        # Load in all our input
        if hostname == None:
            hostname = raw_input("Enter the splunk hostname: (localhost) ")
            if hostname == "":
                hostname = "localhost"
        if username == None:
            username = raw_input("Enter Splunk Admin: (admin) ")
            if username == "":
                username = "admin"
        if password == None:
            password = getpass("Enter Splunk Password: ")
        if app == None:
            app = raw_input(
                "Enter target App Context: (leave blank for user default) ")

        print('%s server="%s:%s" username="%s" app="%s"' %
              (time.time(), hostname, port, username, app))

        try:
            if app:
                self.Service = client.connect(
                    host=hostname, port=port, username=username, password=password, app=app)
            else:
                self.Service = client.connect(
                    host=hostname, port=port, username=username, password=password)

        except Exception as e:
            print('%s %s' % (time.time(), e))
            sys.exit()

    def get_stored_passwords(self, match=".*"):
        print('%s executing="get_stored_passwords" args="%s"' %
              (time.time(), [match]))
        storage_passwords = self.Service.storage_passwords
        pattern = re.compile(match)
        for credential in storage_passwords:
            if pattern.match(credential.name):
                print(credential.name)
                pprint(credential.content)

    def create_new_password(self, username, password, realm=None):

        print('%s executing="create_new_password" args="%s"' %
              (time.time(), [username, "*****", realm]))
        storage_password = self.Service.storage_passwords.create(
            username, password, realm)
        print('%s new_password name="%s" content="%s"' %
              (time.time(), storage_password.name, json.dumps(storage_password.content)))

    def delete_credential(self, username, realm=None, confirm=False):
        if confirm != True:
            confirm = raw_input(
                "Are you sure you want to delete %s:%s? y/N " % (realm, username))
            if confirm != "y":
                return

        print('%s executing="delete_credential" args="%s"' %
              (time.time(), [username, realm]))

        storage_passwords = self.Service.storage_passwords.delete(
            username, realm=realm)
        for credential in storage_passwords:
            if credential.content['username'] == username and credential.content['realm'] == realm:
                print('%s delete_credential status="failure" name="%s" username="%s" realm="%s"' % (
                    time.time(), credential.name, username, realm))
                return

        print('%s delete_credential status="success" username="%s" realm="%s"' % (
            time.time(), username, realm))


def process_cmd(cmd, kwargs=None):

    try:
        if cmd == "g":
            if kwargs == None:
                kwargs = dict()
                kwargs['match'] = raw_input(
                    "Enter the credential regex match: (.*) ")
            Manager.get_stored_passwords(**kwargs)
        elif cmd == "a":
            if kwargs == None:
                kwargs = dict()
                kwargs['username'] = raw_input("Enter the new username: ")
                kwargs['password'] = getpass("Enter the new user password: ")
                kwargs['realm'] = raw_input(
                    "Enter the realm for the new user: ")
            Manager.create_new_password(**kwargs)
        elif cmd == "d":
            if kwargs == None:
                kwargs = dict()
                kwargs['username'] = raw_input("Enter the new username: ")
                kwargs['realm'] = raw_input(
                    "Enter the realm for the new user: ")
            Manager.delete_credential(**kwargs)
    except Exception as e:
        print('%s %s' % (time.time(), e))


if __name__ == "__main__":
    # Arg parsing
    parser = argparse.ArgumentParser()
    parser.add_argument("-s", "--hostname", type=str,
                        help="The hostname of the Splunk Server e.g. localhost")
    parser.add_argument("-P", "--port", type=int,
                        help="The port for the management service", default=8089)
    parser.add_argument("-u", "--username", type=str,
                        help="The username with sufficient perms", default="admin")
    parser.add_argument("-p", "--password", type=str,
                        help="The password for the username supplied")
    parser.add_argument("-f", "--passwordfile", type=str,
                        help="The file (path inclusive) containing the password")
    parser.add_argument("-a", "--app", type=str,
                        help="The app context for the credentials")
    parser.add_argument("-r", "--runfile", type=str,
                        help="File (path inclusive) to read json run config")

    args = parser.parse_args()

    if args.passwordfile:
        try:
            with open(args.passwordfile, 'r') as pf:
                args.password = pf.read().replace('\n', '')
        except Exception as e:
            print("%s Unable to read passwordfile [%s]" % (
                time.time(), args.passwordfile))
            print("%s %s" % (time.time(), e))
            sys.exit()

    Manager = CredentialManager(hostname=args.hostname, port=args.port,
                                username=args.username, password=args.password, app=args.app)

    run_sequence = None
    if args.runfile:
        try:
            with open(args.runfile, 'r') as rf:
                run_sequence = json.loads(rf.read())
        except Exception as e:
            print("%s Unable to read run file [%s]" % (
                time.time(), args.runfile))
            print("%s %s" % (time.time(), e))

    if run_sequence == None:
        cmd = None
        while cmd != "q":

            process_cmd(cmd)

            print("")
            print(
                "-------------------------------------------------------------------------")
            print(
                "Enter your action: g = Get all Creds, a = Add new credential, q = Quit")
            cmd = raw_input()
    else:
        for run in run_sequence:
            if 'args' in run:
                process_cmd(run['cmd'], run['args'])
            else:
                process_cmd(run['cmd'])
