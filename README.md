# Credential Manager

Used to list, create and delete stored passwords in Splunk

## Installation

### Requirements

* Splunk SDK deployed to `./splunk-sdk`
* Python 2.7+
* Splunk...?

## Usage

### Interative

```bash
~/credential-manager $ python ./credential_manager.py -h
```

```bash
usage: credential_manager.py [-h] [-s HOSTNAME] [-P PORT] [-u USERNAME]
                             [-p PASSWORD] [-f PASSWORDFILE] [-a APP]  
                             [-r RUNFILE]

optional arguments:
  -h, --help            show this help message and exit
  -s HOSTNAME, --hostname HOSTNAME
                        The hostname of the Splunk Server e.g. localhost
  -P PORT, --port PORT  The port for the management service
  -u USERNAME, --username USERNAME
                        The username with sufficient perms
  -p PASSWORD, --password PASSWORD
                        The password for the username supplied
  -f PASSWORDFILE, --passwordfile PASSWORDFILE
                        The file (path inclusive) containing the password
  -a APP, --app APP     The app context for the credentials
  -r RUNFILE, --runfile RUNFILE
                        File (path inclusive) to read json run config
```
---

### Providing arguments

#### Example
```bash
~/credential-manager $ python credential_manager.py -s localhost -u admin -f "./pass.wd" --app=""
```

---

### Run file

Set your run sequence of actions in a file in `json` format. The included `runfile.example.json` can be copied to provide a sample runfile.

#### Example

```bash
~/credential-manager $ python ./credential_manager.py -r "./runfile.example.json"
```

